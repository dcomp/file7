# File 7

## Stack Utilizado

O objetivo do teste era entregar o escopo funcional abaixo no menor espaço de tempo possível:
 
*Montar um sistema de controle de arquivos, com crud e upload somente de imagens e pdfs, com controle de versão e feito via painel administrativo com login.*


Para isso foi utilizado:

- Laravel Admin (para área administrativa e autenticação)
- Laravel File Manager (para gestão dos arquivos)
- PHP 7.4 / Laravel 8 / SQLite (para facilitar o setup do ambiente do avaliador)

## Pré Instalação

É necessário que no sistema operacional estejam instaladas as seguintes dependências:

- PHP 7
- Composer 1.1
- Extensão GD
- Extensão SQLite

## Instalação

Após clonar o repositório, na raiz do projeto, executar:

```
composer install
```

Depois, inicializar o webserver do Laravel:

```
php artisan serve
```

Abrir o navegador e acessar aplicação em ```http://localhost:8000/```.

## Usuários de Demonstração

```
Login: usuario1
Senha: usuario1

Login: usuario2
Senha: usuario2

Login: admin
Senha: admin
```


