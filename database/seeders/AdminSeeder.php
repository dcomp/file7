<?php
namespace Database\Seeders;

use Encore\Admin\Auth\Database\Permission;
use Illuminate\Database\Seeder;
use Encore\Admin\Auth\Database\AdminTablesSeeder;
use Encore\Admin\Auth\Database\Administrator;
use Encore\Admin\Auth\Database\Role;

class AdminSeeder extends AdminTablesSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        parent::run();
        Permission::insert([
            [
                'name'        => 'File7',
                'slug'        => 'file7',
                'http_method' => '',
                'http_path'   => '/file7*',
            ],
        ]);
        Role::insert([
            'name' => 'Usuário Comum',
            'slug' => 'user',
        ]);
        Role::find(2)->permissions()->save(Permission::find(6));

        Administrator::create([
            'username' => 'usuario1',
            'password' => bcrypt('usuario1'),
            'name'     => 'Usuario1',
        ]);

        Administrator::find(2)->roles()->save(Role::find(2));

        Administrator::create([
            'username' => 'usuario2',
            'password' => bcrypt('usuario2'),
            'name'     => 'Usuario2',
        ]);
        Administrator::find(3)->roles()->save(Role::find(2));
    }
}
