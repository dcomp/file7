init:
	composer install

	@if [ ! -f .env ];\
		then cp .env.example .env;\
		echo "Copied from .env.example";\
		php artisan key:generate;\
	fi

	touch database/database.sqlite
	php artisan storage:link
	php artisan migrate:refresh
	php artisan db:seed --class=AdminSeeder
	php artisan admin:install
	php artisan vendor:publish --tag=lfm_public --force
	php artisan vendor:publish --tag=lfm_config --force
	php artisan vendor:publish --provider="Encore\Admin\AdminServiceProvider"
	git init
	git reset --hard

update:
	composer update

test:
	@echo "Testing File7 app..."

