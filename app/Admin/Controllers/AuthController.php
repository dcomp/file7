<?php

namespace App\Admin\Controllers;

use Encore\Admin\Controllers\AuthController as BaseAuthController;
use \Admin;

class AuthController extends BaseAuthController
{

    public function redirectTo()
    {
        if (Admin::user()->isRole('user')) {
            return '/file7';
        }

        return '/';
    }

}
